﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UPush.Net.Client;

namespace UPushDemo
{
    public partial class Form1 : Form
    {
        // 配置
        private string ip = "192.168.6.189";
        private int port = 7302;
        private string appId = "bb407166";
        private string appKey = "59f92a086415ee518b78cd4f9bb25fb7";
        private string packageName = "cn.com.upush.net.sdk.app.test";

        private UPushClient client;

        public Form1()
        {
            InitializeComponent();
            // UPushListener在子线程回调，更新UI需要添加以下这行代码，或者在实际开发中可以使用delegate方式
            Control.CheckForIllegalCrossThreadCalls = false;

            client = new UPushClient();
            client.configUPush(packageName, appId, appKey, new UPushListener(this));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ipTextBox.Text = ip;
            portTextBox.Text = port + "";
        }

        // 注册
        private void button1_Click(object sender, EventArgs e)
        {
            client.RegisterUPush(ip, port);
        }

        // 绑定用户
        private void button2_Click(object sender, EventArgs e)
        {
            string userid = userIdTextBox.Text;
            if (userid != null && userid.Length != 0)
            {
                client.BindUser(userid);
            }
        }

        // 暂停
        private void button3_Click(object sender, EventArgs e)
        {
            client.Pause();
        }

        // 恢复
        private void button4_Click(object sender, EventArgs e)
        {
            client.Resume();
        }

        // 注销
        private void button5_Click(object sender, EventArgs e)
        {
            client.UnregisterUPush();
        }

        // UDP
        private void UDP_CheckedChanged(object sender, EventArgs e)
        {
            client.SetNetworkType(0);
        }

        // TCP
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            client.SetNetworkType(1);
        }

        // 清空
        private void button6_Click(object sender, EventArgs e)
        {
            msgTextBox.Text = "";
        }

        public void appendContent(string str)
        {
            string text = msgTextBox.Text;
            if (text.Length == 0)
            {
                msgTextBox.Text = str;
            }
            else
            {
                msgTextBox.Text = text + "\r\n" + str;
            }
        }

        private class UPushListener : IUPushListener
        {
            private Form1 form1;

            public UPushListener(Form1 form1)
            {
                this.form1 = form1;
            }

            public void OnBindUser(int status)
            {
                form1.appendContent(string.Format("[OnBindUser] {0}", status == 0 ? "success" : "fail"));
            }

            public void OnPush(string data, ulong pushTime, bool isOfflineMsg)
            {
                form1.appendContent(string.Format("[OnPush] {0}", ToGB2312(data)));// ToGB2312 解决中文乱码
            }

            public void OnPushToken(int code, string pushToken, string userId)
            {
                if (code == 0)
                {
                    form1.appendContent(string.Format("[OnPushToken] {0},{1}", pushToken, userId == null ? "null" : userId));
                }
                else
                {
                    form1.appendContent(string.Format("[OnPushToken] error {0}", code));
                }

            }

            public void OnRegisteredOnAnotherDevice(string info)
            {
                form1.appendContent(string.Format("[OnRegisteredOnAnotherDevice] {0}", info));
            }

            public void OnRevoke(string data, ulong revokeTime)
            {
                form1.appendContent(string.Format("[OnRevoke] {0}", data));

            }

            public void OnStatusChanged(int status)
            {
                form1.statusLabel.Text = statusText(status);
            }

            public void OnUnregister()
            {
                form1.appendContent("[OnUnregister]");
            }

            private string statusText(int status)
            {
                switch (status)
                {
                    case 0:
                        return "初始";
                    case 1:
                        return "取得连接地址";
                    case 2:
                        return "取得地址失败";
                    case 3:
                        return "正在连接";
                    case 4:
                        return "连接失败";
                    case 5:
                        return "正在登陆";
                    case 6:
                        return "登陆成功";
                    case 7:
                        return "登陆失败";
                    case 8:
                        return "正在关闭";
                    case 9:
                        return "正在注销";
                    case 10:
                        return "注销失败";
                    case 11:
                        return "已经关闭";
                    case 12:
                        return "已注销";
                    default:
                        return "";
                }
            }

            private string ToGB2312(string str)
            {
                try
                {
                    Encoding cp850 = Encoding.GetEncoding("gb2312");
                    byte[] temp = cp850.GetBytes(str);
                    return Encoding.GetEncoding("utf-8").GetString(temp, 0, temp.Length);
                }
                catch (Exception ex)//(UnsupportedEncodingException ex)
                {
                    Console.Write(ex.Message);
                    return null;
                }
            }
        }
    }
}
